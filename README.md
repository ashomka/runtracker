# REST API that tracks jogging times of users


## Users endpoints

create account:
POST /api/users
{
        "username": "usr",
        "password": "pwd"
}

log in:
POST /api/login
{
        "username": "usr",
        "password": "pwd"
}

change password for account usr:
PUT /api/users/usr
{
        "username": "usr",
        "password": "newpwd"
}

delete account usr:
DELETE /api/users/usr

get user info for usr:
GET /api/users/usr

get all users (admin or user manager only):
GET /api/users



## Runs endpoints

add new run:
POST /api/runs
{
	"timestamp" : "2020-02-06T11:32:00+10",
	"username" : "admin",
	"distanceMeters" : 20,
	"durationSeconds" : 30,
	"latitude" : -27.9533299,
	"longitude" : 153.0884787
}

update run 12:
PUT /api/users/usr/runs/12
{
	"id" : "12"
	"timestamp" : "2020-02-06T11:32:00+10",
	"username" : "admin",
	"distanceMeters" : 20,
	"durationSeconds" : 30,
	"latitude" : -27.9533299,
	"longitude" : 153.0884787
}

delete run 12:
DELETE /api/runs/12

get run 12:
GET /api/runs/12

get all runs (admin only):
GET /api/runs

get all runs for user usr:
GET /api/users/usr/runs

get weekly report for user usr:
GET /api/users/usr/report



## Filter and pagination

filter example:
/api/runs?$filter=(date(timestamp) eq 2020-02-06) AND ((distanceMeters gt 4000) OR (distanceMeters lt 2000))

pagination example:
/api/runs?$skip=20&$top=10



## Deployment

1. Update db connection string

Go to appsettings.json file in the project and change connection string to point to your production database.

2. Create the IIS site.

In IIS Manager, open the server's node in the Connections panel. Right-click the Sites folder. Select Add Website.
Provide a Site name and set the Physical path to the app's deployment folder that you created. 
Provide the Binding configuration and create the website by selecting OK.

3. Publish the app.

In a command shell, publish the app in Release configuration with the dotnet publish command:
dotnet publish --configuration Release

4. Deploy the app.
Move the contents of the bin/Release/{TARGET FRAMEWORK}/publish folder to the IIS site folder on the server, which is the site's Physical path in IIS Manager.


