﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RunTracker.Controllers;
using RunTracker.Entities;
using RunTracker.Models;
using RunTracker.Services;

namespace RunTrackerTest
{
    public static class TestMocker
    {
        static readonly string _secret = "ApplicationDbContextApplicationDbContextApplicationDbContextApplicationDbContextApplicationDbContext";
        public static ApplicationDbContext GetInMemoryDbContext()
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            // Create instance of DbContext
            var dbContext = new ApplicationDbContext(options);
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        public static UsersController GetUsersController(ApplicationDbContext dbContext, string role)
        {
            var options = Options.Create(new AppSettings() { SecretKey = _secret });
            var controller = new UsersController(new UserService(options, dbContext));
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = GetUser(role, role) }
            };

            return controller;
        }

        public static UsersController GetUsersController(ApplicationDbContext dbContext)
        {
            var options = Options.Create(new AppSettings() { SecretKey = _secret });
            var controller = new UsersController(new UserService(options, dbContext));
            return controller;
        }

        public static LoginController GetLoginController(ApplicationDbContext dbContext)
        {
            var options = Options.Create(new AppSettings() { SecretKey = _secret });
            var controller = new LoginController(new UserService(options, dbContext));
            return controller;
        }

        public static void CleanupDbContext(ApplicationDbContext dbContext)
        {
            dbContext.Runs.RemoveRange(dbContext.Runs);
            dbContext.Users.RemoveRange(dbContext.Users);
            dbContext.SaveChanges();
        }

        public static RunsController AsUser(this RunsController controller, string username, string role)
        {
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = GetUser(username, role) }
            };
            return controller;
        }

        public static ClaimsPrincipal GetUser(string username, string role)
        {
            return new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, role)
            }, "mock"));
        }

        public static void SeedRuns(ApplicationDbContext dbContext, int count, string username)
        {
            var random = new Random();

            for (int i = 0; i < count; i++)
            {
                int distance = random.Next(500, 10000);
                int duration = distance / 2;
                dbContext.Runs.Add(
                    new Run
                    {
                        Username = username,
                        DistanceMeters = distance,
                        DurationSeconds = duration,
                        Timestamp = DateTime.Now.AddDays(-random.Next(30)),
                        Latitude = -27.9533299,
                        Longitude = 153.0884787,
                        Weather = "Clear 25°C"
                    });
            }
            dbContext.SaveChanges();
        }

        public static void SeedUsers(ApplicationDbContext dbContext, int count, string role)
        {
            var random = new Random();

            for (int i = 0; i < count; i++)
            {
                var username = role + (i+1).ToString();
                dbContext.Users.Add(
                    new User
                    {
                        Username = username,
                        Password = username,
                        Role = role
                    });
            }
            dbContext.SaveChanges();
        }
    }
}
