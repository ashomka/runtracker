using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RunTracker.Entities;
using RunTracker.Models;
using Xunit;

namespace RunTrackerTest
{
    public class UsersUnitTest
    {
        [Fact]
        public async Task Should_Return_All_Users_When_Calling_Get_Without_Parameters()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsAdmin = TestMocker.GetUsersController(dbContext, Roles.Admin);
            int expectedCount = 10;
            TestMocker.SeedUsers(dbContext, expectedCount, Roles.User);

            //Act
            var users = await controllerAsAdmin.GetAll();
            //Assert
            Assert.IsType<OkObjectResult>(users.Result);
            Assert.Equal(expectedCount, ((users.Result as OkObjectResult).Value as IEnumerable<UserViewModel>).Count());
        }

        [Fact]
        public async Task Should_Return_Correct_Username_When_Calling_GetUser()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = TestMocker.GetUsersController(dbContext, Roles.User);
            var username = Roles.User;
            dbContext.Users.Add(
                new User
                {
                    Username = username,
                    Password = username,
                    Role = Roles.User
                });
            dbContext.SaveChanges();

            //Act
            var users = await controllerAsUser.GetUser(username);
            //Assert
            Assert.IsType<OkObjectResult>(users.Result);
            Assert.Equal(username, ((users.Result as OkObjectResult).Value as UserViewModel).Username);
        }

        [Fact]
        public async Task Should_Return_204_When_Calling_PutUser()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = TestMocker.GetUsersController(dbContext, Roles.User);
            var username = Roles.User;
            dbContext.Users.Add(
                new User
                {
                    Username = username,
                    Password = username,
                    Role = Roles.User
                });
            dbContext.SaveChanges();

            //Act
            var newPassword = "newPassword";
            var put = await controllerAsUser.PutUser(username,
                new User
                {
                    Username = username,
                    Password = newPassword,
                    Role = Roles.User
                });

            //Assert
            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Should_Return_204_When_Calling_DeleteUser()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = TestMocker.GetUsersController(dbContext, Roles.User);
            var username = Roles.User;
            dbContext.Users.Add(
                new User
                {
                    Username = username,
                    Password = username,
                    Role = Roles.User
                });
            dbContext.SaveChanges();

            //Act
            var delete = await controllerAsUser.DeleteUser(username);

            //Assert
            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Should_Return_Token_When_Calling_CreateUser_And_Authenticate()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controller = TestMocker.GetUsersController(dbContext);
            var username = Roles.User;
        
            //Act
            var post = await controller.CreateUser(
                new Credentials
                {
                    Username = username,
                    Password = username,
                });

            //Assert
            Assert.IsType<OkResult>(post);

            //Act
            var loginController = TestMocker.GetLoginController(dbContext);
            var jwt = await loginController.Authenticate(
                new Credentials
                {
                    Username = username,
                    Password = username,
                });

            Assert.True(!string.IsNullOrWhiteSpace(jwt.Value.Token));
        }
    }
}
