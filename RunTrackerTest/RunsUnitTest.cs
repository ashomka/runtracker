using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RunTracker.Controllers;
using RunTracker.Entities;
using RunTracker.Models;
using Xunit;

namespace RunTrackerTest
{
    public class RunsUnitTest
    {
        readonly string _adminName = "admin";
        readonly string _userName = "user1";

        [Fact]
        public async Task Should_Return_All_Runs_When_Calling_GetRuns_Without_Parameters()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsAdmin = new RunsController(dbContext).AsUser(_adminName, Roles.Admin);
            int expectedCount = 10;
            TestMocker.SeedRuns(dbContext, expectedCount, _adminName);

            //Act
            var runs = await controllerAsAdmin.GetRuns();
            //Assert
            Assert.Equal(expectedCount, runs.Value.Count());
        }

        [Fact]
        public async Task Should_Return_Users_Runs_When_Calling_GetUserRuns()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = new RunsController(dbContext).AsUser(_userName, Roles.User);
            int expectedCount = 10;
            TestMocker.SeedRuns(dbContext, expectedCount, _userName);
            TestMocker.SeedRuns(dbContext, expectedCount, _adminName);

            //Act
            var runs = await controllerAsUser.GetUserRuns(_userName);
            //Assert
            Assert.Equal(expectedCount, runs.Value.Count());
        }

        [Fact]
        public async Task Should_Return_403_When_Calling_Other_GetUserRuns()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = new RunsController(dbContext).AsUser(_userName, Roles.User);
            int count = 2;
            TestMocker.SeedRuns(dbContext, count, _userName);
            TestMocker.SeedRuns(dbContext, count, _adminName);

            //Act
            var runs = await controllerAsUser.GetUserRuns(_adminName);
            //Assert
            Assert.IsType<ForbidResult>(runs.Result);
        }

        [Fact]
        public async Task Should_Return_Single_Run_When_Calling_PostAndGet()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = new RunsController(dbContext).AsUser(_userName, Roles.User);

            //Act
            var post = await controllerAsUser.PostRun(
                    new Run
                    {
                        Username = _userName,
                        DistanceMeters = 1000,
                        DurationSeconds = 1000,
                        Timestamp = DateTime.Now,
                        Latitude = -27.9533299,
                        Longitude = 153.0884787,
                        Weather = "Clear 25�C"
                    });
            //Assert
            Assert.IsType<CreatedAtActionResult>(post.Result);

            //Act
            var runs = await controllerAsUser.GetUserRuns(_userName);
            //Assert
            Assert.Single(runs.Value);
        }

        [Fact]
        public async Task Should_Return_Updated_Properties_When_Calling_PutAndGet()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = new RunsController(dbContext).AsUser(_userName, Roles.User);
            TestMocker.SeedRuns(dbContext, 1, _userName);

            //Act
            var runs = await controllerAsUser.GetUserRuns(_userName);
            var id = runs.Value.First().Id;
            var weather = "Weather";
            var distance = 1234;
            var duration = 5678;
            var time = DateTime.Now;
            var lat = 123.4;
            var lon = -33.3;

            var put = await controllerAsUser.PutRun(id,
                    new Run
                    {
                        Id = id,
                        Username = _userName,
                        DistanceMeters = distance,
                        DurationSeconds = duration,
                        Timestamp = time,
                        Latitude = lat,
                        Longitude = lon,
                        Weather = weather
                    });
            //Assert
            Assert.IsType<NoContentResult>(put);

            //Act
            var run = await controllerAsUser.GetRun(id);
            //Assert
            Assert.Equal(id, run.Value.Id);
            Assert.Equal(_userName, run.Value.Username);
            Assert.Equal(distance, run.Value.DistanceMeters);
            Assert.Equal(duration, run.Value.DurationSeconds);
            Assert.Equal(time, run.Value.Timestamp);
            Assert.Equal(lat, run.Value.Latitude);
            Assert.Equal(lon, run.Value.Longitude);
            Assert.Equal(weather, run.Value.Weather);
        }

        [Fact]
        public async Task Should_Return_No_Runs_When_Calling_DeleteAndGet()
        {
            //Arrange
            var dbContext = TestMocker.GetInMemoryDbContext();
            var controllerAsUser = new RunsController(dbContext).AsUser(_userName, Roles.User);
            TestMocker.SeedRuns(dbContext, 1, _userName);

            //Act
            var runs = await controllerAsUser.GetUserRuns(_userName);
            var id = runs.Value.First().Id;
            var deleted = await controllerAsUser.DeleteRun(id);
            //Assert
            Assert.Equal(id, deleted.Value.Id);

            //Act
            runs = await controllerAsUser.GetUserRuns(_userName);
            //Assert
            Assert.Empty(runs.Value);
        }
    }
}
