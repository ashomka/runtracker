﻿using Microsoft.EntityFrameworkCore;
using RunTracker.Entities;

namespace RunTracker.Services
{
    public interface IApplicationDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Run> Runs { get; set; }
    }

    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Creates database structure not supported with Annotations using Fluent syntax.
        /// </summary>
        /// <param name="optionsBuilder">The configuration interface.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Run>().HasIndex(
                run => new { run.Username }).IsUnique(false);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Run> Runs { get; set; }
    }
}
