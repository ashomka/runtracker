using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RunTracker.Entities;
using RunTracker.Models;
using RunTracker.Helpers;

namespace RunTracker.Services
{
    public interface IUserService
    {
        Task<string> Authenticate(Credentials credentials);
        Task<IEnumerable<UserViewModel>> GetAllAsync();
        Task<User> GetByUsernameAsync(string username);
        Task CreateAsync(Credentials credentials);
        Task<bool> UserExistsAsync(string username);
        Task<bool> DeleteAsync(string username);
        Task UpdateAsync(User user);
    }

    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings, ApplicationDbContext dbContext)
        {
            _appSettings = appSettings.Value;
            _dbContext = dbContext;
        }

        public async Task<string> Authenticate(Credentials credentials)
        {
            var user = await GetByUsernameAsync(credentials.Username);
            if (user == null)
            {
                return null;
            }

            if (! await Task.Run(() => PasswordHash.Verify(credentials.Password, user.Password)))
            {
                return null;
            }

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddHours(24),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = await Task.Run(() => tokenHandler.CreateToken(tokenDescriptor));
            var result = tokenHandler.WriteToken(token);
            return result;
        }

        public async Task<IEnumerable<UserViewModel>> GetAllAsync()
        {
            return await _dbContext.Users.Select(u => new UserViewModel(u)).ToListAsync();
        }

        public async Task<User> GetByUsernameAsync(string username)
        {
            var user = await _dbContext.Users.FindAsync(username);
            return user;
        }

        public async Task CreateAsync(Credentials credentials)
        {
            var hash = await Task.Run(() => PasswordHash.Calculate(credentials.Password));
            var user = new User
            {
                Username = credentials.Username,
                Password = hash,
                Role = Roles.User
            };
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> UserExistsAsync(string username)
        {
            return await _dbContext.Users.AnyAsync(e => e.Username == username);
        }

        public async Task<bool> DeleteAsync(string username)
        {
            var user = await _dbContext.Users.FindAsync(username);
            if (user == null)
            {
                return false;
            }

            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task UpdateAsync(User user)
        {
            _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();
        }
    }
}