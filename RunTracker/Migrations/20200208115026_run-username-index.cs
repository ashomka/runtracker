﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RunTracker.Migrations
{
    public partial class runusernameindex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Username",
                table: "Runs",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Runs_Username",
                table: "Runs",
                column: "Username");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Runs_Username",
                table: "Runs");

            migrationBuilder.AlterColumn<string>(
                name: "Username",
                table: "Runs",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
