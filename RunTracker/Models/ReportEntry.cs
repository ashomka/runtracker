﻿using System;

namespace RunTracker.Models
{
    public class ReportEntry
    {
        public string UserName { get; set; }

        public DateTime WeekStartUtc { get; set; }

        public int DistanceMeters { get; set; }

        public int DurationSeconds{ get; set; }
    }
}
