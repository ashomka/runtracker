using RunTracker.Entities;

namespace RunTracker.Models
{
    public class UserViewModel
    {
        public UserViewModel(User user)
        {
            Username = user.Username;
            Role = user.Role;
        }

        public string Username { get; set; }
    
        public string Role { get; set; }
    }
}