namespace RunTracker.Models
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string UserManager = "UserManager";
        public const string User = "User";
    }
}