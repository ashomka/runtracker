﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace RunTracker.Helpers
{
    public static class PasswordHash
    {		        
		public static string Calculate(string input)
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            string hash = CalculateWithSalt(input, salt);

            var saltEncoded = Convert.ToBase64String(salt);

            return saltEncoded + "." + hash;
        }

        internal static bool Verify(string provided, string stored)
        {
            var tokens = stored.Split('.');
            var salt = tokens[0];
            var storedHash = tokens[1];
            var providedHash = CalculateWithSalt(provided, Convert.FromBase64String(salt));
            return storedHash == providedHash;
        }

        private static string CalculateWithSalt(string input, byte[] salt)
        {
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: input,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }
    }
}
