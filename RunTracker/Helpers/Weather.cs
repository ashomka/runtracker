﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace RunTracker.Helpers
{
    public static class Weather
    {
        public static async Task<string> GetSummary(double lat, double lon, DateTime dateTime)
        {
            // Gold Coast: -27.9533299,153.0884787
            var apiKey = "ce07f60de2d66c282c66664b16d66fa6";
            var time = dateTime.ToString("s");
            var url = $"https://api.darksky.net/forecast/{apiKey}/{lat},{lon},{time}?exclude=hourly,minutely,daily,alerts,flags&units=si";
            var result = "";

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "RunTracker/1");

                var json = await httpClient.GetStringAsync(new Uri(url));
                dynamic response = JObject.Parse(json);
                result = response.currently.summary;
                if (double.TryParse(Convert.ToString(response.currently.temperature), out double temperature))
                {
                    result += string.Format($", {(int)temperature}°C");
                }
            }
            return result;
        }
    }
}
