﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RunTracker.Entities;
using RunTracker.Helpers;
using RunTracker.Models;
using RunTracker.Services;

namespace RunTracker.Controllers
{
    [Authorize(Roles = Roles.Admin + "," + Roles.User)]
    [Route("api/[controller]")]
    [ApiController]
    public class RunsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RunsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // filter example:
        // https://localhost:44383/api/runs?$filter=(date(timestamp) eq 2020-02-06) AND ((distanceMeters gt 40) OR (distanceMeters lt 20))
        // pagination example
        // https://localhost:44383/api/runs?$skip=20&$top=10
        [HttpGet]
        [Authorize(Roles = Roles.Admin)]
        [EnableQuery(PageSize = 1000)]
        public async Task<ActionResult<IEnumerable<Run>>> GetRuns()
        {
            return await _context.Runs.ToListAsync();
        }

        [HttpGet("/api/users/{username}/runs")]
        [EnableQuery(PageSize = 1000)]
        public async Task<ActionResult<IEnumerable<Run>>> GetUserRuns(string username)
        {
            if (!IsOwnerOrAdmin(username))
            {
                return Forbid();
            }

            return await _context.Runs.Where(r => r.Username == username).ToListAsync();
        }

        [HttpGet("/api/users/{username}/report")]
        [EnableQuery(PageSize = 1000)]
        public async Task<ActionResult<IEnumerable<ReportEntry>>> GetUserReport(string username)
        {
            if (!IsOwnerOrAdmin(username))
            {
                return Forbid();
            }

            var runs = await _context.Runs.Where(r => r.Username == username).ToListAsync();

            // group by week starting Sunday
            var query = from r in runs
            group r by new { r.Username, WeekStartUtc = r.Timestamp.Date.AddDays(-(int)r.Timestamp.DayOfWeek) } into g
            select new ReportEntry
            {
                UserName = g.Key.Username,
                WeekStartUtc = g.Key.WeekStartUtc,
                DistanceMeters = (int)Math.Round(g.Average(x => x.DistanceMeters)),
                DurationSeconds = (int)Math.Round(g.Average(x => x.DurationSeconds))
            };

            return await Task.FromResult(query.ToList());
        }

        // GET: api/Runs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Run>> GetRun(long id)
        {
            var run = await _context.Runs.FindAsync(id);
            if (run == null)
            {
                return NotFound();
            }

            if (!IsOwnerOrAdmin(run.Username))
            {
                return Forbid();
            }

            return run;
        }

        // PUT: api/Runs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRun(long id, Run modifiedRun)
        {
            if (id != modifiedRun.Id)
            {
                return BadRequest();
            }

            var run = await _context.Runs.FindAsync(id);
            if (run == null)
            {
                return NotFound();
            }

            if (!IsOwnerOrAdmin(run.Username))
            {
                return Forbid();
            }

            run.DistanceMeters = modifiedRun.DistanceMeters;
            run.DurationSeconds = modifiedRun.DurationSeconds;
            run.Timestamp = modifiedRun.Timestamp;
            run.Latitude = modifiedRun.Latitude;
            run.Longitude = modifiedRun.Longitude;
            run.Weather = modifiedRun.Weather;

            _context.Update(run);       
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Runs
        [HttpPost]
        public async Task<ActionResult<Run>> PostRun(Run run)
        {
            if (!IsOwnerOrAdmin(run.Username))
            {
                return Forbid();
            }

            run.Weather = await Weather.GetSummary(run.Latitude, run.Longitude, run.Timestamp);
            await _context.Runs.AddAsync(run);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRun", new { id = run.Id }, run);
        }

        // DELETE: api/Runs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Run>> DeleteRun(long id)
        {
            var run = await _context.Runs.FindAsync(id);
            if (run == null)
            {
                return NotFound();
            }

            if (!IsOwnerOrAdmin(run.Username))
            {
                return Forbid();
            }

            _context.Runs.Remove(run);
            await _context.SaveChangesAsync();

            return run;
        }

        private bool IsOwnerOrAdmin(string username)
        {
            var currentUsername = User.Identity.Name;
            return username == currentUsername || User.IsInRole(Roles.Admin);
        }
    }
}
