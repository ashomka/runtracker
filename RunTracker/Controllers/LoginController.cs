﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RunTracker.Models;
using RunTracker.Services;

namespace RunTracker.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<Jwt>> Authenticate([FromBody]Credentials credentials)
        {
            var token = await _userService.Authenticate(credentials);

            if (token == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }

            return new Jwt { Token = token };
        }
    }
}