﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RunTracker.Entities;
using RunTracker.Models;
using RunTracker.Services;

namespace RunTracker.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // POST api/users
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> CreateUser([FromBody]Credentials credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await _userService.UserExistsAsync(credentials.Username))
            {
                return BadRequest(new { message = "Username not available" });
            }

            if (string.IsNullOrWhiteSpace(credentials.Password))
            {
                return BadRequest(new { message = "Empty password not allowed" });
            }

            await _userService.CreateAsync(credentials);

            return Ok();
        }

        // GET: api/Users
        [HttpGet]
        [Authorize(Roles = Roles.Admin + "," + Roles.UserManager)]
        [EnableQuery(PageSize = 1000)]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> GetAll()
        {
            return Ok(await _userService.GetAllAsync());
        }

        // GET: api/Users/5
        [HttpGet("{username}")]
        public async Task<ActionResult<UserViewModel>> GetUser(string username)
        {
            if (!IsOwnerOrAdmin(username))
            {
                return Forbid();
            }

            var user = await _userService.GetByUsernameAsync(username);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(new UserViewModel(user));
        }

        // PUT: api/Users/5
        [HttpPut("{username}")]
        public async Task<IActionResult> PutUser(string username, User modifiedUser)
        {
            if (!IsOwnerOrAdmin(username))
            {
                return Forbid();
            }

            if (username != modifiedUser.Username)
            {
                return BadRequest();
            }

            var user = await _userService.GetByUsernameAsync(username);
            if (user == null)
            {
                return NotFound();
            }

            if (IsAdminAndNotOwner(username))
            {
                user.Role = modifiedUser.Role;
            }

            if (!string.IsNullOrWhiteSpace(modifiedUser.Password))
            {
                user.Password = modifiedUser.Password;
            }

            await _userService.UpdateAsync(user);

            return NoContent();
        }

        // DELETE: api/Users/5
        [HttpDelete("{username}")]
        public async Task<IActionResult> DeleteUser(string username)
        {
            if (!IsOwnerOrAdmin(username))
            {
                return Forbid();
            }

            if (! await _userService.DeleteAsync(username))
            {
                return NotFound();
            }

            return NoContent();
        }

        private bool IsOwnerOrAdmin(string username)
        {
            var currentUsername = User.Identity.Name;
            return username == currentUsername || User.IsInRole(Roles.Admin) || User.IsInRole(Roles.UserManager);
        }
        private bool IsAdminAndNotOwner(string username)
        {
            var currentUsername = User.Identity.Name;
            return username != currentUsername && User.IsInRole(Roles.Admin);
        }
    }
}
