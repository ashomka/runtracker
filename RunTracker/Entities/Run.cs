﻿using System;

namespace RunTracker.Entities
{
    public class Run
    {
        public long Id { get; set; }

        public string Username { get; set; }

        public DateTime Timestamp { get; set; }

        public int DistanceMeters { get; set; }

        public int DurationSeconds{ get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Weather { get; set; }
    }
}
